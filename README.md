# Andrena Multichain

This is multichain-in-a-docker-container for use with the Andrena project.

### Ports exposed:

- 6700 - internode network connections
- 10601 - RPC API

## Build

To build the Docker image locally, run:

`dc build`

## Running the container

Use docker-compose to start up the service "multichain".\
It will expose the ports listed above on your localhost for you to connect to.
All data will be stored in a Docker Volume named `andrena-multichain_multichaindata`, so the chain will persist.

`docker-compose up -d`

## CLI

To easily connect to the `multichain-cli` on the running container:

`./cli.sh`
