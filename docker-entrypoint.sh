#!/bin/bash

set -e

rpcuser=${RPCUSER:-andrena}
rpcpassword=${RPCPASSWORD:-andrena}
rpcallowip=${RPCALLOWIP:-0.0.0.0/0.0.0.0}

# Set variables
sed -i "s|^rpcuser.*|rpcuser=$(printf "%q" "$rpcuser")|" /root/.multichain/andrena/multichain.conf
sed -i "s|^rpcpassword.*|rpcpassword=$(printf "%q" "$rpcpassword")|" /root/.multichain/andrena/multichain.conf
sed -i "s|^rpcallowip.*|rpcallowip=$(printf "%q" "$rpcallowip")|" /root/.multichain/andrena/multichain.conf

exec "$@"
